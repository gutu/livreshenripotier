package fr.app.henripotier.henripotier;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by nelly on 04/05/2016.
 */
public class Util {

    public static String BASKET = "";
    public static JSONArray bookList = new JSONArray();
    public static Integer basketLength = 0;
    public static void setBadgeCount(Context context, LayerDrawable icon, String count) {
        BadgeDrawable badge; // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge); //getting the layer 2
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        }
        else {
            badge = new BadgeDrawable(context);
        }
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }
}

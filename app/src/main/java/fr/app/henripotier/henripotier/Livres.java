package fr.app.henripotier.henripotier;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Livres extends Fragment {

    private View rootView;
    private List<LivreInfo> livres;
    private OnFragmentInteractionListener mListener;
    private static String TAG = "BOOKS";
    private  RecyclerView rv;
    private LivreAdapter adapter;

    public Livres() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_livres, container, false);

        setHasOptionsMenu(true);

        rv = (RecyclerView)rootView.findViewById(R.id.rv);
        rv.setHasFixedSize(true);

        GridLayoutManager llm = new GridLayoutManager(getActivity(), 2);
        rv.setLayoutManager(llm);
        livres = new ArrayList<>();
        adapter = new LivreAdapter(livres);
        rv.setAdapter(adapter);
        request();

        return  rootView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
    private void request(){
        String tag = "books";
        String url = "http://henri-potier.xebia.fr/books";

        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Chargement...");
        pDialog.show();

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                        livres = new ArrayList<>();
                        for(Integer i = 0; i<response.length(); i++){
                            try {
                               JSONObject book = response.getJSONObject(i);
                                String title = book.getString("title");
                                String isbn = book.getString("isbn");
                                String price = book.getString("price");
                                String cover = book.getString("cover");

                                livres.add(new LivreInfo(isbn, title, price, cover));

                                adapter = new LivreAdapter(livres);
                                rv.setAdapter(adapter);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });
        AppController.getInstance().addToRequestQueue(req, tag);
    }
}

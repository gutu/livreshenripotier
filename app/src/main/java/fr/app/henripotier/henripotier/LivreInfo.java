package fr.app.henripotier.henripotier;

/**
 * Created by nelly on 04/05/2016.
 */
public class LivreInfo {

    protected String isbn;
    protected String title;
    protected String cover;
    protected String price;

    public LivreInfo(String isbn, String title, String price, String cover) {
        this.isbn = isbn;
        this.cover = cover;
        this.price = price;
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

package fr.app.henripotier.henripotier;

/**
 * Created by nelly on 07/05/2016.
 */

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;


import java.util.List;

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.BasketViewHolder> {

    List<LivreInfo> livres;

    BasketAdapter(List<LivreInfo> livres) {
        this.livres = livres;
    }

    @Override
    public int getItemCount() {
        return livres.size();
    }

    @Override
    public BasketViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.basket_card, viewGroup, false);
        BasketViewHolder pvh = new BasketViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final BasketViewHolder basketViewHolder, final int i) {

        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        imageLoader.get(livres.get(i).cover, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("IMG", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    basketViewHolder.vCover.setImageBitmap(response.getBitmap());
                }
            }
        });
        basketViewHolder.vTitle.setText(livres.get(i).title);
        basketViewHolder.vPrice.setText(livres.get(i).price+" €");

    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class BasketViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView vTitle, vPrice;
        ImageView vCover;
        FloatingActionButton fab;
        RelativeLayout vlayout;

        BasketViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.card_view);
            vCover = (ImageView) itemView.findViewById(R.id.cover);
            vTitle = (TextView) itemView.findViewById(R.id.title);
            vPrice = (TextView) itemView.findViewById(R.id.price);
            fab = (FloatingActionButton) itemView.findViewById(R.id.fab);
            vlayout = (RelativeLayout) itemView.findViewById(R.id.expandable);
        }

    }
}



package fr.app.henripotier.henripotier;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Basket extends AppCompatActivity {

    private List<LivreInfo> livres;
    private static String TAG = "Offers";
    private RecyclerView rv;
    private BasketAdapter adapter;
    private List<String> offers;
    private TextView vArticles, vTotal, vReduc, vDeduc, vSlice, vCaisse;
    private String percentage = "", minus = "", slice = "", sliceValue = "";
    private Double prixTotal = null, prixCaisse = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(Basket.this);

                alertDialog.setTitle("Commande");
                alertDialog.setMessage("Votre commande a bien été prise en compte");
                alertDialog.setIcon(R.drawable.ic_add_shopping_cart_white);
                alertDialog.setPositiveButton(
                        "Payer",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Basket.this, HomePage.class);
                                startActivity(intent);
                                finish();
                                Util.BASKET = "";
                                Util.bookList = new JSONArray();
                                Util.basketLength = 0;
                                dialog.dismiss();
                            }
                        }
                );
                alertDialog.show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        vArticles = (TextView) findViewById(R.id.nb);
        vTotal = (TextView) findViewById(R.id.total);
        vReduc = (TextView) findViewById(R.id.reduc);
        vDeduc = (TextView) findViewById(R.id.minus);
        vSlice = (TextView) findViewById(R.id.slice);
        vCaisse = (TextView) findViewById(R.id.caisse);

        rv = (RecyclerView)findViewById(R.id.rv);
        rv.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(Basket.this);
        rv.setLayoutManager(llm);
        livres = new ArrayList<>();
        adapter = new BasketAdapter(livres);
        rv.setAdapter(adapter);
        if(Util.basketLength < 1){
            vArticles.setText("Votre panier est vide");
            vReduc.setVisibility(View.GONE);
            vTotal.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }else{
            fab.setVisibility(View.VISIBLE);
            setAdapter(Util.bookList);
        }


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {

                Util.bookList.remove(viewHolder.getAdapterPosition());
                Util.basketLength = Util.basketLength-1;
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                setAdapter(Util.bookList);
                if(Util.basketLength == 0){
                    Intent i = new Intent(Basket.this, HomePage.class);
                    startActivity(i);
                    finish();
                    Util.basketLength = 0;
                    Util.BASKET  ="";
                    Util.bookList = new JSONArray();
                }

            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;
                    Paint p = new Paint();

                    if(dX < 0){
                        p.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public void onMoved(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int fromPos, RecyclerView.ViewHolder target, int toPos, int x, int y) {
                super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rv);

    }
    private void setAdapter(JSONArray array){

        Resources res = getResources();
        String sNb = String.format(res.getString(R.string.nb_articles), Util.basketLength);
        vArticles.setText(sNb);

        livres = new ArrayList<>();
        offers = new ArrayList<>();

            try {
                Double sum = 0d;
                for(Integer i = 0; i<array.length(); i++){
                    JSONObject book = array.getJSONObject(i);
                    String title = book.getString("title");
                    String isbn = book.getString("isbn");
                    String price = book.getString("price");
                    String cover = book.getString("cover");
                    livres.add(new LivreInfo(isbn, title, price, cover));
                    offers.add(isbn);
                    sum += Double.parseDouble(price);
                }
                String list = "";
                for(String book: offers){
                    list += book +",";
                }
                getOffers(list);
                prixTotal = sum;
                adapter = new BasketAdapter(livres);
                rv.setAdapter(adapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

    private void getOffers(String isbn){
        String tag = "offers";
        String url = "http://henri-potier.xebia.fr/books";

        final ProgressDialog pDialog = new ProgressDialog(Basket.this);
        pDialog.setMessage("Chargement...");
        pDialog.show();

        JsonObjectRequest req = new JsonObjectRequest(url+"/"+isbn+"/commercialOffers", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        pDialog.hide();
                        try {
                            Resources res = getResources();
                            JSONArray array = response.getJSONArray("offers");
                            System.out.println("valeurs array " + array);
                            for(Integer i = 0; i<array.length(); i++){
                                JSONObject offer_type = array.getJSONObject(i);
                                String type = offer_type.isNull("type") ? "" : offer_type.getString("type");
                                String value = offer_type.isNull("value") ? "" : offer_type.getString("value");

                                if(type.equals("percentage")){
                                    percentage = value;
                                    String sReduc = String.format(res.getString(R.string.reduc), percentage+"%");
                                    vReduc.setVisibility(View.VISIBLE);
                                    vReduc.setText(sReduc);
                                    Double cal = prixTotal*Double.parseDouble(percentage) / 100;
                                    prixTotal = prixTotal - cal;
                                }else if(type.equals("slice")){
                                    slice = value;
                                    sliceValue = offer_type.isNull("sliceValue") ? "" : offer_type.getString("sliceValue");
                                    if(prixTotal >= Double.parseDouble(sliceValue)){
                                        prixTotal = prixTotal - Double.parseDouble(slice);
                                        String sReduc = String.format(res.getString(R.string.slice), slice);
                                        vSlice.setVisibility(View.VISIBLE);
                                        vSlice.setText(sReduc);
                                    }else{
                                        vSlice.setVisibility(View.GONE);
                                        Log.i("R", "Remboursement non applicable");
                                    }
                                }else if(type.equals("minus")){
                                    minus = value;
                                    String sReduc = String.format(res.getString(R.string.deduction), minus);
                                    vDeduc.setText(sReduc);
                                    vDeduc.setVisibility(View.VISIBLE);
                                }
                            }
                            String sTotal = String.format(res.getString(R.string.total), prixTotal);
                            vTotal.setVisibility(View.VISIBLE);
                            vTotal.setText(sTotal);

                            if(!minus.isEmpty()){
                                prixCaisse = prixTotal - Double.parseDouble(minus);
                                String sCaisse = String.format(res.getString(R.string.prix_caisse), prixCaisse);
                                vCaisse.setText(sCaisse);
                                vCaisse.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                pDialog.hide();
            }
        });
        AppController.getInstance().addToRequestQueue(req, tag);
    }

}

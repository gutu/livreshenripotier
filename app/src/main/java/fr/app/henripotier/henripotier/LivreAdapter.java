package fr.app.henripotier.henripotier;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UTFDataFormatException;
import java.util.List;

/**
 * Created by nelly on 04/05/2016.
 */
public class LivreAdapter extends RecyclerView.Adapter<LivreAdapter.LivreViewHolder> {

    List<LivreInfo> livres;

    LivreAdapter(List<LivreInfo> livres) {
        this.livres = livres;
    }

    @Override
    public int getItemCount() {
        return livres.size();
    }

    @Override
    public LivreViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_livre, viewGroup, false);
        LivreViewHolder pvh = new LivreViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(final LivreViewHolder livreViewHolder, final int i) {

        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        imageLoader.get(livres.get(i).cover, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("IMG", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    livreViewHolder.vCover.setImageBitmap(response.getBitmap());
                }
            }
        });
        livreViewHolder.vTitle.setText(livres.get(i).title);
        livreViewHolder.vPrice.setText(livres.get(i).price+" €");

//        livreViewHolder.fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(v.getContext(), Panier.class);
//                v.getContext().startActivity(i);
//            }
//        });
        livreViewHolder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                Util.BASKET += livres.get(i).isbn+",";
                JSONObject book = new JSONObject();
                try {
                    book.put("isbn", livres.get(i).isbn);
                    book.put("title", livres.get(i).title);
                    book.put("price", livres.get(i).price);
                    book.put("cover", livres.get(i).cover);
                    Util.bookList.put(book);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Util.basketLength++;
                HomePage.isChangeMenu = true;

                if (v.getContext() instanceof HomePage) {
                            ((HomePage) v.getContext()).onPrepareOptionsMenu(HomePage.menu);
                }

                AlertDialog.Builder alertDialog = new AlertDialog.Builder((Activity) v.getContext());

                alertDialog.setTitle("Panier");
                alertDialog.setMessage("Ce livre a bien été ajouté à votre panier");
                alertDialog.setIcon(R.drawable.ic_add_shopping_cart_white);

                alertDialog.setNegativeButton(
                        "Panier",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(v.getContext(), Basket.class);
                                v.getContext().startActivity(i);
                            }
                        }
                );

                alertDialog.setPositiveButton(
                        "Continuer",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }
                );
                alertDialog.show();

            }
//                 livreViewHolder.vLlayout.setVisibility(View.VISIBLE);
//                livreViewHolder.descLayout.setVisibility(View.GONE);
//                livreViewHolder.fab.setVisibility(View.GONE);
//                Intent i = new Intent(v.getContext(), Basket.class);
//                v.getContext().startActivity(i);
        });


    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class LivreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView cv;
        TextView vTitle, vPrice;
        ImageView vCover;
        FloatingActionButton fab;
        RelativeLayout vlayout;
        LinearLayout vLlayout, descLayout;
        Button vCancel, vConfirm;

        LivreViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.card_view);
            vCover = (ImageView) itemView.findViewById(R.id.cover);
            vTitle = (TextView) itemView.findViewById(R.id.title);
            vPrice = (TextView) itemView.findViewById(R.id.price);
            fab = (FloatingActionButton) itemView.findViewById(R.id.fab);
            vlayout = (RelativeLayout) itemView.findViewById(R.id.expandable);
            //vLlayout = (LinearLayout) itemView.findViewById(R.id.linear);
           // descLayout = (LinearLayout) itemView.findViewById(R.id.desc);
            //itemView.setOnClickListener(this);
        }

        private void expand() {
            //set Visible
            vlayout.setVisibility(View.VISIBLE);

            final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            vlayout.measure(widthSpec, heightSpec);

            ValueAnimator mAnimator = slideAnimator(0, vlayout.getMeasuredHeight());
            mAnimator.start();
        }

        private void collapse() {
            int finalHeight = vlayout.getHeight();

            ValueAnimator mAnimator = slideAnimator(finalHeight, 0);

            mAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    //Height=0, but it set visibility to GONE
                    vlayout.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }

            });
            mAnimator.start();
        }

        private ValueAnimator slideAnimator(int start, int end) {

            ValueAnimator animator = ValueAnimator.ofInt(start, end);

            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    //Update Height
                    int value = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = vlayout.getLayoutParams();
                    layoutParams.height = value;
                    vlayout.setLayoutParams(layoutParams);
                }
            });
            return animator;
        }

        @Override
        public void onClick(View v) {
            if (vlayout.getVisibility() == View.GONE) {
                expand();
            } else {
                collapse();
            }
        }
    }
}


